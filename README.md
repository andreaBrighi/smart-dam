# Progetto #3 - Smart Dam 

Si vuole realizzare un sistema IoT che implementi una versione semplificata di un sistema intelligente per il monitoraggio del livello idrometrico (*) di un fiume e controllo di una diga (dam).
In particolare, il sistema ha il principale compito di monitorare il livello dell’acqua in un punto del fiume e, in caso di situazione critica, controllare l’apertura di una diga per far defluire l’acqua (in apposite vasche di contenimento o nei campi).

(*) livello idrometrico -- per "livello idrometrico" in un determinato luogo del fiume si intende il dislivello tra la superficie dell'acqua di un fiume ed un punto di riferimento altimetrico, che può essere il livello medio del mare (l.m.m) oppure lo "zero" dell'idrometro stesso (detto "zero idrometrico").
L'idrometro è lo strumento che rileva le quote idrometriche, cioè il livello dell'acqua, dei fiumi o dei laghi.

Il sistema complessivamente è costituito da 5 sottosistemi:



- Remote Hydrometer (ESP) - abbreviato a seguire in RH
	sistema embedded che ha la funzione di monitorare il livello idrometrico del fiume (si presuppone in un punto specifico) e interagire con la parte Service via HTTP oppure MQTT
- Dam Service (backend - pc) - abbreviato a seguire in DS
	servizio REST che funge da centralina di controllo del sistema. Interagisce via seriale con il Controller (arduino) e via HTTP o MQTT con il Remote Hydrometer (ESP) e con la Dashboard (frontend/PC). 
- Dam Controller (Arduino) - abbreviato a seguire in DC
	sistema embedded che controlla la diga. Interagisce via seriale con il Service e via Bluetooth con la parte Mobile App, usata dagli operatori.
- Dam App (Android - smartphone) abbreviato a seguire in DM
	Mobile app che ad un operatore di poter agire direttamente sulla diga, essendo nelle prossimità. Interagisce con la parte Controller (via Bluetooth) ed eventualmente con la parte Service (via HTTP). 
	Dam Dashboard (Frontend - web app o Client su PC) abbreviato a seguire in DD
	Front end per visualizzazione/monitoraggio dati.


Dettaglio componenti HW di RH e DC:

## Remote Hydrometer 
SoC ESP 8266 o ESP 32 con board che include:

- Un led verde L alarm
- 1 sonar per misurare distanza

## Dam Controller 
Microcontrollore Arduino UNO con board che include:

- un verde Ls 
- 1 servomotore  per simulare apertura della diga
- 1 modulo Bluetooth HC-06 o HC-05


Comportamento dettagliato del sistema 

Il sistema (complessivamente) si può trovare in tre stati:

- NORMALE
- PRE-ALLARME
- ALLARME

Lo stato NORMALE corrisponde alla situazione in cui il livello idrometrico L misurato dal sottosistema RH è inferiore al livello L1, ovvero la distanza D misurata dal sonar è superiore a D1; lo stato PRE-ALLARME corrisponde a un livello misurato compreso fra L1 e L2 (distanza D compresa fra D2 e D1); lo stato di ALLARME a un livello superiore o uguale a L2 (distanza misurata dal sonar D inferiore o uguale a D2).

Quando il sistema si trova in stato NORMALE, nel sottosistema RH il led Lalarm è spento e non viene inviato nessun dato.

Quando il sistema si trova in stato PRE-ALLARME, nel sottosistema RH il led Lalarm lampeggia (blinking) e il livello L misurato viene inviato a DS con una frequenza pari a Freq1.
Lato DS, quanto il sistema si trova in stato PRE-ALLARME, devono essere memorizzati i valori di L inviati da RH, associati ad una marcatura temporale. 

Quando il sistema si trova in stato di ALLARME, nel sottosistema RH il led Lalarm viene mantenuto acceso senza lampeggiare e il livello misurato viene inviato a DS con una frequenza pari a Freq2 ( > Freq1).
Lato DS, quando il sistema si trova in stato ALLARME, oltre a continuare la memorizzazione dei dati inviati da RH, viene gestita l’apertura della diga, interagendo con il sistema DC. Il livello di apertura della diga  è rappresentato da un valore percentuale P, per cui P = 0% significa diga chiusa e P = 100% diga completamente aperta.
P può assumere 6 valori: 0, 20, 40, 60, 80, 100. Il valore di P dipende da quanto il livello L ha superato L2, per cui:  

- L < L2  (ovvero D > D2) corrisponde a P = 0, ovvero diga chiusa
- L2 <= L < L2 + DeltaL  (ovvero D2 - DeltaD < D <= D2) corrisponde a P = 20%
- L2 + DeltaL <= L < L2 + 2*DeltaL (ovvero D2 - 2*DeltaD < D <= D2 - DeltaD) corrisponde a P = 40%
- L2 + 2*DeltaL <= L < L2 + 3*DeltaL (ovvero D2 - 3*DeltaD < D <= D2 - 2*DeltaD)  corrisponde a P = 60%
- L2 + 3*DeltaL <= L < L2 + 4*DeltaL (ovvero D2 - 4*DeltaD < D <= D2 - 3*DeltaD)  corrisponde a P = 80% 
- L >= L2 + 4*DeltaL (ovvero D <= D2 - 4*DeltaD)  corrisponde a P = 100%, diga completamente aperta.

Nel sottosistema DC il dispositivo che attua l’apertura della diga è il servomotore, in cui la posizione del rotore (0-180 gradi) è determinata dal livello di apertura P, in modo proporzionale.
Nel sottosistema DC, quando il sistema si trova in stato NORMALE o PRE-ALLARME il led Ls è spento, mentre lampeggia (blinking) quando è in ALLARME. 

Per ciò concerne il sottosistema DM, l’applicazione deve anzitutto consentire di visualizzare lo stato in cui si trova il sistema (se NORMALE, PRE-ALLARME, ALLARME), l’ultima rilevazione del livello L (se in PRE-ALLARME o ALLARME) e il livello di apertura della diga P (se in ALLARME).
Inoltre, l’app deve consentire di entrare in una modalità MANUALE di controllo della diga. 
L’utente può selezionare tale modalità solo se il sistema si trova in stato di ALLARME. In modalità MANUALE, l’operatore deve poter controllare e gestire il livello P di apertura, monitorando il livello dell’acqua L.
Nel sottosistema DC, quando la diga è in modalità MANUALE, il led Ls deve essere acceso senza lampeggiare.

Lato dashboard DD, quando il sistema si trova in stato NORMALE, viene visualizzato solo l’indicazione dello stato. Quando il sistema si trova in stato PRE-ALLARME, oltre allo stato, viene visualizzato l’andamento del livello L nel tempo, considerando le ultime Nril rilevazioni.
Quando il sistema si trova in stato ALLARME, oltre a stato e andamento, viene visualizzato anche il livello di apertura P della diga (l’ultimo valore).
Se la diga si trova in modalità di controllo MANUALE, tale modalità deve essere indicata (insieme all’indicazione dello stato).

La comunicazione fra RH e DS si basa su protocollo HTTP
L’applicazione DM deve interagire via seriale con DC e può o meno interagire via HTTP con DS

Realizzare il sistema con le seguenti specifiche:
- Remote Hydrometer RH basato su piattaforma ESP (o equivalenti)
- nessun vincolo sulla tecnologia da usare
- Usare protocollo HTTP per comunicare con Dam Service
- Dam Controller  DC basato su piattaforma Arduino
- Implementare la logica in termini di macchine a stati finiti 
- Comunica via seriale con DS
- Dam Service DS  in esecuzione su un PC
Può essere implementata come web app (quindi basato su protocollo HTTP) con la tecnologia che si ritiene più opportuna o anche come client usando socket TCP o UDP
è possibile usare qualsiasi libreria (per visualizzazione andamenti)

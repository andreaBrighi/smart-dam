const ip = require("ip");
const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline');
const express = require('express'),
    app = express(),
    port = process.env.PORT || 8080;

var net = require('net');

let sPort;

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

SerialPort.list().then(
    ports => ports.forEach(function (i) {
        if (i.path.includes("usbmodem")) {
            sPort = new SerialPort(i.path, { baudRate: 115200, autoOpen: false });
            sPort.open(function (err) {
                if (err) {
                    return console.log('Error opening port: ', err.message)
                }
                const parser = sPort.pipe(new Readline({ delimiter: '\n' }))

                const routes = require('./api/routes/Routes');

                routes(app, parser, sPort);

                app.listen(port, ip.address(), () => {
                    console.log(`Server Api listening on ${ip.address()}:${port}`)
                });

                var HOST = ip.address();
                var PORT = 8081;

                net.createServer(function (sock) {
                    console.log('Connected: ' + sock.remoteAddress + ':' + sock.remotePort);
                    sock.on('data', function (data) {

                        data = data.toString().replace("\n", "$\n");
                        console.log(data);
                        sPort.write(data);
                    });
                    sock.on('close', function (data) {
                        console.log('Closed: ' + sock.remoteAddress + ' ' + sock.remotePort);
                    });
                }).listen(PORT, HOST);
                console.log("Emulator's socket on " + HOST + ":" + PORT);
            });
        }
    }),
    err => console.error(err)
);  
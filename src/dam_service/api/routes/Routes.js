'use strict';
module.exports = function (app, inPort, outPort) {

  app.route('/api/status')
    .get(getStatus)
    .put(updateStatus);


  app.route('/api/value')
    .get(getValues)
    .post(postValue);

  app.route('/api/opening')
    .get(getOpening);

  app.route('/api/control')
    .get(getControl);

  inPort.on('data', readSerial);

  const DeltaL = 0.2;
  const L2 = 3;
  const max = 20;
  let values = [];
  let status = "no";
  let opening = 0;
  let control = "a";


  function getStatus(req, res) {
    res.json({ status: status });
  }

  function updateStatus(req, res) {
    status = req.body.status;
    outPort.write("status: " + status + "\n");
    res.send();
  }

  function getOpening(req, res) {
    res.json({ opening: opening });
  }


  function getValues(req, res) {
    res.json(values);
  }


  function postValue(req, res) {
    let value = req.body.value;
    values.push({ time: new Date().toString(), value: value });
    if (values.length > max) {
      values.shift();
    }
    if (status == "al" && control == "a") {
      let tmp = 0;
      if (value < L2) {
        tmp = 0;
      }
      else if (L2 <= value && value < L2 + DeltaL) {
        tmp = 20;
      }
      else if (L2 + DeltaL <= value && value < L2 + 2 * DeltaL) {
        tmp = 40;
      }
      else if (L2 + 2 * DeltaL <= value && value < L2 + 3 * DeltaL) {
        tmp = 60;
      }
      else if (L2 + 3 * DeltaL <= value && value < L2 + 4 * DeltaL) {
        tmp = 80;
      }
      else if (L2 + 4 * DeltaL <= value) {
        tmp = 100;
      }
      if (opening != tmp) {
        opening = tmp;
        outPort.write("opening: " + opening + "\n");
      }

    }
    res.send();
  }

  function getControl(req, res) {
    res.json({ control: control });
  }

  function readSerial(data) {
    data = data.toString();
    if (status == "al") {
      if (data.indexOf("control: a") >= 0) {
        control = "a";
      }
      else if (data.indexOf("control: m") >= 0) {
        control = "m";
      }
      else if (data.indexOf("opening: ") >= 0) {
        opening = data.replace("opening: ", "");
      }
    }
  }

};


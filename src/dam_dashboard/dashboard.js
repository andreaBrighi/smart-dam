$(document).ready(function () {
    const time = 30;
    const status = $("#status");
    const opening = $("#opening");
    const lblOpening = $("#lblOpening");
    const manual = $("#manual");
    const sectChart = $("sector + sector");
    const ctx = document.getElementById('chart').getContext('2d');
    const ip = "192.168.244.254";
    const chart = new Chart(ctx, {
        type: 'line',
        data: {
            datasets: [{
                label: 'livello',
                backgroundColor: 'rgba(0, 225, 255, 0.8)',
                borderColor: 'rgba(55, 165, 240, 1)',
                pointBorderColor: 'rgba(60, 170, 250, 1)',
                borderWidth: 2
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
    getData();
    const timer = setInterval(getData, time);

    function getData() {
        $.getJSON(`http://${ip}:8080/api/status`, function (result) {
            if (result.status == "no") {
                manual.hide();
                opening.hide();
                lblOpening.hide();
                sectChart.hide();
                status.val("normale");
            } else if (result.status == "pa") {
                manual.hide();
                opening.hide();
                sectChart.show();
                lblOpening.hide();
                status.val("pre-allarme");
            } else if (result.status == "al") {
                opening.show();
                sectChart.show();
                lblOpening.show();
                status.val("allarme");
                $.getJSON(`http://${ip}:8080/api/control`, function (res) {
                    if (res.controllo == "m") {
                        manual.show();
                    } else {
                        manual.hide();
                    }
                });
                $.getJSON(`http://${ip}:8080/api/opening`, function (res) {
                    opening.val(res.opening);
                });
            }
            if (result.status != "n") {
                $.getJSON(`http://${ip}:8080/api/value`, function (res) {
                    chart.data.labels = [];
                    chart.data.datasets.forEach((dataset) => {
                        dataset.data = [];
                    });
                    res.forEach((obj) => {
                        chart.data.labels.push(obj.time);
                    });
                    chart.data.datasets.forEach((dataset) => {
                        res.forEach((obj) => {
                            dataset.data.push(obj.value);
                        });
                    });
                    chart.update();
                });
            }
        });
    }

});
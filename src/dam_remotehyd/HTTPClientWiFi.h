#ifndef __HTTP_CLIENT_WIFI__
#define __HTTP_CLIENT_WIFI__

#include "Arduino.h"

class HTTPClientWiFi
{
public:
    HTTPClientWiFi();
    void connectWiFi(String ssidName, String pwd);
    void setServer(String address);
    int sendValue(float value);
    int updateState(String state);
    bool connected();

protected:
    String address;
};

#endif
#ifndef __SONAR__
#define __SONAR__

#define NO_OBJ_DETECTED -1

class Sonar
{

public:
  Sonar(int echoPin, int trigPin);
   enum precisione
  {
    MAX,
    NORMAL,
    MIN
  };
  float getDistance(precisione p);
 

private:
  const float vs = 331.5 + 0.6 * 20;

  int echoPin, trigPin;
};

#endif

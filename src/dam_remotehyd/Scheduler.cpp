#include "Scheduler.h"
#include <Ticker.h>

volatile bool timerFlag;
Ticker ticker;

void timerHandler()
{
  timerFlag = true;
}

void Scheduler::init(int basePeriod)
{
  this->basePeriod = basePeriod;
  timerFlag = false;
  int period = basePeriod;
  ticker.attach_ms(period, timerHandler);
  nTasks = 0;
}

bool Scheduler::addTask(Task *task)
{
  if (nTasks < MAX_TASKS - 1)
  {
    taskList[nTasks] = task;
    nTasks++;
    return true;
  }
  else
  {
    return false;
  }
}

void Scheduler::schedule()
{
  if (timerFlag)
  {
    timerFlag = false;
    for (int i = 0; i < nTasks; i++)
    {
      if (taskList[i]->isActive())
      {
        if (taskList[i]->updateAndCheckTime(basePeriod))
        {
          taskList[i]->tick();
        }
      }
    }
  }
}

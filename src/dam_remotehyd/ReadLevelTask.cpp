#include "ReadLevelTask.h"
#include "Light.h"
#include "Sonar.h"
#include "HTTPClientWiFi.h"
#include "BlinkingTask.h"
#include "config.h"
#include "Arduino.h"

ReadLevelTask::ReadLevelTask(Light *pLed, Sonar *pSonar, HTTPClientWiFi *pClient)
{
    this->pLed = pLed;
    this->pSonar = pSonar;
    this->pClient = pClient;
    this->pBlink = new BlinkingTask(pLed);
    this->pBlink->init(BLINK_TIME);
    this->pBlink->setActive(false);
    this->state = NORMAL;
    int code = this->pClient->updateState("no");
}

void ReadLevelTask::setActive(bool active)
{
    ReadLevelTask::setActive(active);
}

void ReadLevelTask::tick()
{
    float d;
    float l;
    switch (this->state)
    {
    case NORMAL:
    {
        d = this->pSonar->getDistance(this->pSonar->MAX);
        l = MAX_DISTANCE - d;
        if (d != -1)
        {
            if (d < 2 && d >= 1)
            {
                this->state = PRE_ALERT;
                this->pBlink->setActive(true);
                this->init(PRE_ALLARM_TIME);
                int code = this->pClient->updateState("pa");
                code = this->pClient->sendValue(l);
            }
            else if (d < 1)
            {
                this->state = ALERT;
                this->pBlink->setActive(false);
                this->init(ALLARM_TIME);
                this->pLed->switchOn();
                int code = pClient->updateState("al");
                code = pClient->sendValue(l);
            }
        }
        break;
    }
    case PRE_ALERT:
    {
        if (this->pBlink->updateAndCheckTime(getPeriod()))
        {
            this->pBlink->tick();
        }
        d = this->pSonar->getDistance(this->pSonar->NORMAL);
        l = MAX_DISTANCE - d;
        if (d >= 2 || d == -1)
        {
            this->state = NORMAL;
            this->pBlink->setActive(false);
            this->init(NORMAL_TIME);
            this->pLed->switchOff();
            int code = this->pClient->updateState("no");
        }
        else if (d < 1)
        {
            this->state = ALERT;
            this->pBlink->setActive(false);
            this->init(ALLARM_TIME);
            this->pLed->switchOn();
            int code = this->pClient->updateState("al");
            code = this->pClient->sendValue(l);
        }
        else
        {
            int code = this->pClient->sendValue(l);
        }
        break;
    }
    case ALERT:
    {
        d = this->pSonar->getDistance(this->pSonar->MIN);
        l = MAX_DISTANCE - d;
        if (d >= 1 || d == -1)
        {
            this->state = PRE_ALERT;
            this->pBlink->setActive(true);
            this->init(PRE_ALLARM_TIME);
            int code = this->pClient->updateState("pa");
        }
        if (d != -1)
        {
            int code = this->pClient->sendValue(l);
        }
        break;
    }
    }
}

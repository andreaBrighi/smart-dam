#include "Sonar.h"

#include "Arduino.h"

Sonar::Sonar(int echoP, int trigP) : echoPin(echoP), trigPin(trigP)
{
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
}

float Sonar::getDistance(precisione p)
{
  unsigned long timeOut;
  switch (p)
  {
  case MAX:
    timeOut = 25000; // 4m
    break;
  case NORMAL:
    timeOut = 12000; // 2m
    break;

  case MIN:
    timeOut = 6000; // 1m
    break;
  }
  digitalWrite(trigPin, LOW);
  delayMicroseconds(3);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(5);
  digitalWrite(trigPin, LOW);

  float tUS = pulseIn(echoPin, HIGH, timeOut);
  if (tUS == 0)
  {
    return NO_OBJ_DETECTED;
  }
  else
  {
    
    float t = tUS / 1000.0 / 1000.0 / 2;
    float d = t * vs;
    return d;
  }
}

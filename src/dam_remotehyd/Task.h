#ifndef __TASK__
#define __TASK__

class Task
{

public:
  Task()
  {
    active = false;
  }

  virtual void init(int period)
  {
    myPeriod = period;
    active = true;
    timeElapsed = 0;
  }

  virtual void tick() = 0;

  bool updateAndCheckTime(int basePeriod)
  {
    timeElapsed += basePeriod;
    if (timeElapsed >= myPeriod)
    {
      timeElapsed = 0;
      return true;
    }
    else
    {
      return false;
    }
  }

  bool isActive()
  {
    return active;
  }

  virtual void setActive(bool active)
  {
    timeElapsed = 0;
    this->active = active;
  }

  int getPeriod()
  {
    return myPeriod;
  }

private:
  int myPeriod;
  int timeElapsed;
  bool active;
};

#endif

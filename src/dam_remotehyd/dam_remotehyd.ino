#include "HTTPClientWiFi.h"
#include "config.h"
#include "Light.h"
#include "Led.h"
#include "Sonar.h"
#include "Task.h"
#include "Scheduler.h"
#include "ReadLevelTask.h"

Scheduler sched;
void setup()
{
    HTTPClientWiFi *clients = new HTTPClientWiFi();
    clients->connectWiFi(ssid, psw);
    clients->setServer(server);
    Light *led = new Led(L_PIN);
    Sonar *sonar = new Sonar(SONAR_ECHO_PIN, SONAR_TRIG_PIN);

    Task *readLevel = new ReadLevelTask(led, sonar, clients);

    readLevel->init(NORMAL_TIME);
    sched.init(SCHED_TIME);
    sched.addTask(readLevel);
}

void loop()
{
    sched.schedule();
}

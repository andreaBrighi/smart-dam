#include "HTTPClientWiFi.h"
#include "Arduino.h"
#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>

HTTPClientWiFi::HTTPClientWiFi()
{
}

void HTTPClientWiFi::connectWiFi(String ssidName, String pwd)
{
    WiFi.begin(ssidName, pwd);
    // Serial.print("Connecting...");
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        // Serial.print(".");
    }
    // Serial.println("Connected: \n local IP: " + WiFi.localIP());
};
void HTTPClientWiFi::setServer(String address)
{
    this->address = address;
};

bool HTTPClientWiFi::connected()
{
    return WiFi.status() == WL_CONNECTED;
};

int HTTPClientWiFi::sendValue(const float value)
{
    HTTPClient http;
    http.begin(address + "/api/value");
    http.addHeader("Content-Type", "application/json");
    String msg =
        String("{ \"value\": ") + String(value) + " }";
    int retCode = http.POST(msg);
    http.end();

    // String payload = http.getString();
    // Serial.println(payload);
    return retCode;
};

int HTTPClientWiFi::updateState(const String state)
{
    HTTPClient http;
    http.begin(address + "/api/status");
    http.addHeader("Content-Type", "application/json");
    String msg =
        String("{ \"status\": \"") + state + "\" }";
    int retCode = http.PUT(msg);
    http.end();

    // String payload = http.getString();
    // Serial.println(payload);
    return retCode;
};

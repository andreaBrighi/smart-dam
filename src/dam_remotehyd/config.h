#ifndef __CONFIG__
#define __CONFIG__

#define SONAR_TRIG_PIN 14 //D5
#define SONAR_ECHO_PIN 12 //D6

#define L_PIN 4 // D2

#define MAX_DISTANCE 4

#define NORMAL_TIME 60
#define PRE_ALLARM_TIME 40
#define ALLARM_TIME 30
#define BLINK_TIME 120
#define SCHED_TIME 10

#define ssid "Test"
#define psw "testtest"
#define server "http://192.168.244.254:8080"

#endif

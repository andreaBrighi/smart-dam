#ifndef __READLEVELTASK__
#define __READLEVELTASK__

#include "Task.h"
#include "Light.h"
#include "Sonar.h"
#include "HTTPClientWiFi.h"

class ReadLevelTask : public Task
{

public:
    ReadLevelTask(Light *pLed, Sonar *pSonar, HTTPClientWiFi *pClient);
    void setActive(bool active);
    void tick();

private:
    enum
    {
        NORMAL,
        PRE_ALERT,
        ALERT
    } state;
    Light *pLed;
    Sonar *pSonar;
    HTTPClientWiFi *pClient;
    Task *pBlink;
};

#endif

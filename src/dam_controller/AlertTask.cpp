#include "AlertTask.h"
#include "MsgService.h"
#include "Light.h"
#include "servo_motor.h"
#include "Msg.h"
#include "Patterns.h"
#include "Arduino.h"
#include "BlinkingTask.h"
#include "config.h"

Pattern *p = new PatternControl();
Pattern *a = new PatternOpening();

AlertTask::AlertTask(Light *pLed,
                     ServoMotor *pServo)
{
    this->pLed = pLed;
    this->pServo = pServo;
    this->opening = 0;
    pTask = new BlinkingTask(pLed);
    pTask->init(BLINK_TIME);
}

void AlertTask::setActive(bool active)
{
    Task::setActive(active);
    if (active)
    {
        pServo->on();
        pServo->setPosition(0);
        pServo->setPosition(opening);
        control = AUTOMATIC;
        msgServiceSerial.sendMsg("control: a");
        pTask->setActive(true);
    }
    else
    {
        pServo->setPosition(0);
        pServo->off();
    }
}

void AlertTask::tick()
{

    Msg *msg = NULL;
    if (msgServiceBT.isMsgAvailable(p))
    {
        msg = msgServiceBT.receiveMsg();
        if (msg->getContent().indexOf("control: m") >= 0)
        {
            control = MANUAL;
            msgServiceSerial.sendMsg("control: m");
            delete msg;
            msg = NULL;
            pTask->setActive(false);
            pLed->switchOn();
        }
        else if (msg->getContent().indexOf("control: a") >= 0)
        {
            control = AUTOMATIC;
            msgServiceSerial.sendMsg("control: a");
            delete msg;
            msg = NULL;
            pTask->setActive(true);
        }
    }
    switch (control)
    {
    case MANUAL:
        if (msgServiceBT.isMsgAvailable(a))
        {   
            msg = msgServiceBT.receiveMsg();
        }
        break;
    case AUTOMATIC:
        if (msgServiceSerial.isMsgAvailable(a))
        {
            msg = msgServiceSerial.receiveMsg();
        }

        break;

    default:
        break;
    }
    if (msg != NULL)
    {
        String tmp = msg->getContent();
        tmp.replace("opening: ", "");
        if (tmp == "0" || tmp == "20" || tmp == "40" || tmp == "60" || tmp == "80" || tmp == "100")
        {
            opening = tmp.toInt();
            pServo->setPosition(18 * opening / 10);
            if (control == MANUAL)
            {
                msgServiceSerial.sendMsg("opening: " + String(this->opening));
            }
        }
        delete msg;
    }
    if (this->pTask->isActive())
    {
        if (this->pTask->updateAndCheckTime(getPeriod()))
        {
            this->pTask->tick();
        }
    }
    
}

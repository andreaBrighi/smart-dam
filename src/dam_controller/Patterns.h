#ifndef __PATTERNS__
#define __PATTERNS__

#include "Arduino.h"
#include "Msg.h"

class PatternStatus :public  Pattern
{
public:
    boolean match(const Msg *m)
    {
        return m->getContent().indexOf("status: ") >= 0;
    };
};

class PatternControl : public Pattern
{
public:
    boolean match(const Msg *m)
    {
        return m->getContent().indexOf("control: ") >= 0;
    };
};

class PatternOpening : public Pattern
{
public:
    boolean match(const Msg *m)
    {
        return m->getContent().indexOf("opening: ") >= 0;
    };
};

#endif

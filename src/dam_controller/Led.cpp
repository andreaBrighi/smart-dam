#include "Led.h"
#include "Arduino.h"

Led::Led(int pin){
  this->pin = pin;
  pinMode(pin,OUTPUT);
  isOn=false;
}

void Led::switchOn(){
  digitalWrite(pin,HIGH);
  isOn=true;
}

void Led::switchOff(){
  digitalWrite(pin,LOW);
  isOn=false;
};

 bool Led::isSwitchOn(){
  return isOn;    
 };

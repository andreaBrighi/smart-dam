#include "config.h"
#include "MsgService.h"
#include "Light.h"
#include "Led.h"
#include "servo_motor.h"
#include "servo_motor_impl.h"
#include "Scheduler.h"
#include "Task.h"
#include "controllerTask.h"

Light *pLed = new Led(L_PIN);
ServoMotor *pServo = new ServoMotorImpl(SERVO_PIN);
Task *pController = new ControllerTask(pLed, pServo);

Scheduler sched;

void setup()
{
    msgServiceSerial.init();
    msgServiceBT.init();
    sched.init(TASK_TIME);
    pController->init(TASK_TIME);
    pController->setActive(true);
    sched.addTask(pController);
}

void loop()
{
    sched.schedule();
}

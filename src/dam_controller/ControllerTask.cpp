#include "ControllerTask.h"
#include "MsgService.h"
#include "Light.h"
#include "servo_motor.h"
#include "Msg.h"
#include "Patterns.h"
#include "AlertTask.h"
#include "Arduino.h"

Pattern* s=new PatternStatus();

ControllerTask::ControllerTask(Light *pLed,
                               ServoMotor *pServo)
{
    
    this->pLed = pLed;
    this->pServo = pServo;
    this->pAlert = new AlertTask( pLed, pServo);
    status = NORMAL;
}

void ControllerTask::init(int period)
{
    Task::init(period);
    this->pAlert->init(period);
    this->pAlert->setActive(false);
};

void ControllerTask::setActive(bool active)
{
    Task::setActive(active);
}

void ControllerTask::tick()
{
    if (msgServiceSerial.isMsgAvailable(s))
    {
        Msg *msg = msgServiceSerial.receiveMsg();
        if (msg->getContent().indexOf("status: al")>=0)
        {
            status = ALERT;
            this->pAlert->setActive(true);
        }
        else
        {
            status = NORMAL;
            pLed->switchOff();
            this->pAlert->setActive(false);
        }
    }
    if (this->pAlert->isActive())
    {
        if (this->pAlert->updateAndCheckTime(getPeriod()))
        {
            this->pAlert->tick();
        }
    }
}

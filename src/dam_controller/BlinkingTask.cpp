#include "BlinkingTask.h"

BlinkingTask::BlinkingTask(Light *pLed)
{
  this->pLed = pLed;
  if (pLed->isSwitchOn())
  {
    state = ON;
  }
  else
  {
    state = OFF;
  }
}

void BlinkingTask::setActive(bool active)
{
  Task::setActive(active);
  if (active)
  {
    if (pLed->isSwitchOn())
    {
      state = ON;
    }
    else
    {
      state = OFF;
    }
  }
  else
  {
    pLed->switchOff();
  }
}

void BlinkingTask::tick()
{
  switch (state)
  {
  case ON:
  {
    pLed->switchOn();
    state = OFF;
    break;
  }
  case OFF:
  {
    pLed->switchOff();
    state = ON;
    break;
  }
  }
}

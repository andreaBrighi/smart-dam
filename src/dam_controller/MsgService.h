#ifndef __MSGSERVICE__
#define __MSGSERVICE__

#include "Arduino.h"
#include "SoftwareSerial.h"
#include "Msg.h"

class MsgServiceSerial
{

public:
  Msg *currentMsg;
  bool msgAvailable;
  

  void init();

  bool isMsgAvailable();
  Msg *receiveMsg();

  bool isMsgAvailable(Pattern *pattern);
  Msg *receiveMsg(Pattern *pattern);

  void sendMsg(const String &msg);
};

class MsgServiceBT
{

public:
  MsgServiceBT(int rxPin, int txPin);
  void init();
  bool isMsgAvailable();
  Msg *receiveMsg();
  bool isMsgAvailable(Pattern *pattern);
  Msg *receiveMsg(Pattern *pattern);
  bool sendMsg(Msg msg);

  Msg *currentMsg;
  bool msgAvailable;

  SoftwareSerial *channel;
};

extern MsgServiceSerial msgServiceSerial;
extern MsgServiceBT msgServiceBT;

#endif

#ifndef __ALLARMTASK__
#define __ALLARMTASK__

#include "Task.h"
#include "Light.h"
#include "servo_motor.h"

class AlertTask : public Task
{

public:
    AlertTask(Light *pLed,
              ServoMotor *pServo);
    void setActive(bool active);
    void tick();

private:
    Light *pLed;
    ServoMotor *pServo;
    enum
    {
        AUTOMATIC,
        MANUAL

    } control;
    int opening;
    Task *pTask;
};

#endif

#ifndef __MSG__
#define __MSG__

#include "Arduino.h"

class Msg
{
    String content;

public:
    Msg(String content)
    {
        this->content = content;
    }

    String getContent()
    {
        return content;
    }
};

class Pattern
{
public:
    virtual boolean match(const Msg *m) = 0;
};

#endif

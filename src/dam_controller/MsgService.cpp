#include "Arduino.h"
#include "MsgService.h"
#include "SoftwareSerial.h"
#include "Msg.h"
#include "config.h"

String content;
String contentb;

MsgServiceSerial msgServiceSerial;
MsgServiceBT msgServiceBT(RX_PIN, TX_PIN);

bool MsgServiceSerial::isMsgAvailable()
{
  return msgAvailable;
}

Msg *MsgServiceSerial::receiveMsg()
{
  if (msgAvailable)
  {
    Msg *msg = currentMsg;
    msgAvailable = false;
    currentMsg = NULL;
    content = "";
    return msg;
  }
  else
  {
    return NULL;
  }
}

void MsgServiceSerial::init()
{
  Serial.begin(115200);
  content.reserve(256);
  content = "";
  currentMsg = NULL;
  msgAvailable = false;
}

void MsgServiceSerial::sendMsg(const String &msg)
{
  Serial.println(msg);
}

void serialEvent()
{
  /* reading the content */
  while (Serial.available())
  {
    char ch = (char)Serial.read();
    if (ch == '\n')
    {
      if (content.length() > 0)
      {
        int index = content.indexOf('$');
        if (index != -1)
        {
          content = content.substring(0, index);
          msgServiceBT.currentMsg = new Msg(content);
          msgServiceBT.msgAvailable = true;
        }
        else
        {
          msgServiceSerial.currentMsg = new Msg(content);
          msgServiceSerial.msgAvailable = true;
        }
      }
      content = "";
    }
    else
    {
      content += ch;
    }
  }
}

bool MsgServiceSerial::isMsgAvailable(Pattern *pattern)
{
  return (msgAvailable && pattern->match(msgServiceSerial.currentMsg));
}

Msg *MsgServiceSerial::receiveMsg(Pattern *pattern)
{
  if (msgAvailable && pattern->match(msgServiceSerial.currentMsg))
  {
    return this->receiveMsg();
  }
  else
  {
    return NULL;
  }
}

MsgServiceBT::MsgServiceBT(int rxPin, int txPin)
{
  channel = new SoftwareSerial(rxPin, txPin);
}

void MsgServiceBT::init()
{
  contentb.reserve(256);
  channel->begin(9600);
  currentMsg = NULL;
}

bool MsgServiceBT::sendMsg(Msg msg)
{
  channel->println(msg.getContent());
}

bool MsgServiceBT::isMsgAvailable()
{
  while (channel->available())
  {
    char ch = (char)channel->read();
    if (ch == '\n')
    {
      msgAvailable = true;
      currentMsg = new Msg(contentb);
      contentb = "";
      return true;
    }
    else
    {
      contentb += ch;
    }
  }
  return msgAvailable;
}

Msg *MsgServiceBT::receiveMsg()
{
  if (msgAvailable)
  {
    Msg *msg = currentMsg;
    currentMsg = NULL;
    msgAvailable = false;
    return msg;
  }
  else
  {
    return NULL;
  }
}

bool MsgServiceBT::isMsgAvailable(Pattern *pattern)
{
  return (isMsgAvailable() && pattern->match(currentMsg));
}

Msg *MsgServiceBT::receiveMsg(Pattern *pattern)
{
  if (msgAvailable && pattern->match(currentMsg))
  {
    return this->receiveMsg();
  }
  else
  {
    return NULL;
  }
}

#ifndef __CONTROLLERTASK__
#define __CONTROLLERTASK__

#include "Task.h"
#include "Light.h"
#include "servo_motor.h"

class ControllerTask : public Task
{

public:
    ControllerTask(Light *pLed,
                   ServoMotor *pServo);
    void init(int period);
    void setActive(bool active);
    void tick();

private:
    Light *pLed;
    ServoMotor *pServo;
    enum
    {
        NORMAL,
        ALERT,
    } status;
    Task *pAlert;
};

#endif

package unibo.btclient;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;

import unibo.btclient.utils.C;
import unibo.btlib.BluetoothChannel;
import unibo.btlib.BluetoothUtils;
import unibo.btlib.ConnectToBluetoothServerTask;
import unibo.btlib.ConnectionTask;
import unibo.btlib.RealBluetoothChannel;
import unibo.btlib.exceptions.BluetoothDeviceNotFound;

public class MainActivity extends AppCompatActivity {

    private BluetoothChannel btChannel;
    private final String ip="192.168.244.254";
    private String control="";
    private String status="";

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();

        if(btAdapter != null && !btAdapter.isEnabled()){
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), C.bluetooth.ENABLE_BT_REQUEST);
        }
        initUI();
        Handler h = new MyHandler(this.getResources(),Looper.getMainLooper(),findViewById(R.id.textViewStatusSystem),findViewById(R.id.textViewType), findViewById(R.id.textViewApertura), findViewById(R.id.textViewActual), findViewById(R.id.sendBtn), findViewById(R.id.changeBtn));
        Thread thread = new Thread(){
            @Override
            public void run() {
                super.run();
                while (true){
                    try {
                        JSONObject obj = doGet("http://"+ip+":8080/api/status");
                        String value = obj.getString("status");
                        status=value;
                        Bundle b = new Bundle();
                        b.putString(MyHandler.VALUE , value);
                        Message msg = new Message();
                        msg.what = MyHandler.VALUES.STATUS.ordinal();
                        msg.setData(b);
                        h.sendMessage(msg);
                        if(value.equals("al")){
                            obj = doGet("http://"+ip+":8080/api/control");
                            value = obj.getString("control");
                            control=value;
                            b = new Bundle();
                            b.putString(MyHandler.VALUE , value);
                            msg = new Message();
                            msg.what = MyHandler.VALUES.CONTROL.ordinal();
                            msg.setData(b);
                            h.sendMessage(msg);
                            obj = doGet("http://"+ip+":8080/api/opening");
                            value = obj.getString("opening");
                            b = new Bundle();
                            b.putString(MyHandler.VALUE , value);
                            msg = new Message();
                            msg.what = MyHandler.VALUES.OPEN.ordinal();
                            msg.setData(b);
                            h.sendMessage(msg);

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        thread.start();


    }

    private void initUI() {
        findViewById(R.id.connectBtn).setOnClickListener(v -> {
            try {
                connectToBTServer();
            } catch (BluetoothDeviceNotFound bluetoothDeviceNotFound) {
                bluetoothDeviceNotFound.printStackTrace();
            }
        });

        findViewById(R.id.changeBtn).setOnClickListener(v -> {

            String message;
            if(control.equals("m")){
                message="control: a\n";
            }
            else {
                message="control: m\n";

            }
            sendMessage(message);

        });

        findViewById(R.id.sendBtn).setOnClickListener(v -> {
            String message = ((EditText)findViewById(R.id.textViewApertura)).getText().toString();
            int i=Integer.parseInt(message);
            if( i<=100 && i>=0 && i%20==0) {
                sendMessage(message);
            }
            else{
                Toast toast = Toast.makeText(getApplicationContext(), "errore valori ammessi: 0, 20, 40, 60, 80, 100", Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();

        btChannel.close();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        if(requestCode == C.bluetooth.ENABLE_BT_REQUEST && resultCode == RESULT_OK){
            Log.d(C.APP_LOG_TAG, "Bluetooth enabled!");
        }

        if(requestCode == C.bluetooth.ENABLE_BT_REQUEST && resultCode == RESULT_CANCELED){
            Log.d(C.APP_LOG_TAG, "Bluetooth not enabled!");
        }
    }

    private void connectToBTServer() throws BluetoothDeviceNotFound {
        final BluetoothDevice serverDevice = BluetoothUtils.getPairedDeviceByName(C.bluetooth.BT_DEVICE_ACTING_AS_SERVER_NAME);

        final UUID uuid = BluetoothUtils.getEmbeddedDeviceDefaultUuid();
        //final UUID uuid = BluetoothUtils.generateUuidFromString(C.bluetooth.BT_SERVER_UUID);

        new ConnectToBluetoothServerTask(serverDevice, uuid, new ConnectionTask.EventListener() {
            @Override
            public void onConnectionActive(final BluetoothChannel channel) {

                ((TextView) findViewById(R.id.statusLabel)).setText(String.format(getString(R.string.status_connected) +" %s",
                        serverDevice.getName()));

                findViewById(R.id.connectBtn).setEnabled(false);

                btChannel = channel;
                btChannel.registerListener(new RealBluetoothChannel.Listener() {
                    @Override
                    public void onMessageReceived(String receivedMessage) {

                    }

                    @Override
                    public void onMessageSent(String sentMessage) {
                    }
                });
            }

            @Override
            public void onConnectionCanceled() {
                ((TextView) findViewById(R.id.statusLabel)).setText(String.format(R.string.status_unable_connected + "%s",
                        C.bluetooth.BT_DEVICE_ACTING_AS_SERVER_NAME));
            }
        }).execute();
    }

    private void sendMessage(final String message){
        new Thread(() -> btChannel.sendMessage(message)).start();
    }

    JSONObject doGet(String sUrl) throws Exception {
        URL url = new URL(sUrl);
        HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
        urlConnection.setRequestMethod("GET");
        int statusCode = urlConnection.getResponseCode();
        if (statusCode ==  200) {
            InputStream it = new BufferedInputStream(urlConnection.getInputStream());
            InputStreamReader read = new InputStreamReader(it);
            BufferedReader buff = new BufferedReader(read);
            StringBuilder dta = new StringBuilder();
            String chunks ;
            while((chunks = buff.readLine()) != null)
            {
                dta.append(chunks);
            }
            return new JSONObject(dta.toString());
        }
        throw new Exception();
    }
}

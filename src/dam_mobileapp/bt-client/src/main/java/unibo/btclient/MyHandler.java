package unibo.btclient;

import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MyHandler extends Handler {
    public enum VALUES
    {
        STATUS,
        CONTROL,
        OPEN
    }
    public static final String VALUE = "value";
    private final TextView textViewState;
    private final TextView textViewType;
    private final EditText editTextOpen;
    private final TextView editTextOpenActual;
    private final Button btnSend;
    private final Button btnChange;
    private final Resources resources;
    public MyHandler(Resources resources, Looper looper, TextView tvS, TextView tvT, EditText etO, TextView editTextOpenActual, Button btnSend, Button btnChange){
        super(looper);
        this.textViewState = tvS;
        this.textViewType = tvT;
        this.editTextOpen=etO;
        this.resources = resources;
        this.editTextOpenActual = editTextOpenActual;
        this.btnSend = btnSend;
        this.btnChange = btnChange;
    }

    @Override
    public void handleMessage(Message msg) {
        String string = msg.getData().getString(VALUE);
        VALUES v= VALUES.values()[msg.what];
        switch(v){
            case STATUS:
                String status="";
                assert string != null;
                if(string.equals("al")){
                    status="allarme";
                }
                else if(string.equals("pa")){
                    status="pre-allarme";
                }
                else if(string.equals("no")){
                    status="normale";
                }
                this.textViewState.setText(resources.getString(R.string.status, status));
                btnChange.setEnabled(string.equals("al"));

                break;
            case CONTROL:
                assert string != null;
                if(string.equals("a")) {
                    this.textViewType.setText(R.string.automatic);
                    this.editTextOpen.setEnabled(false);
                    this.btnSend.setEnabled(false);
                }
                else{
                    this.textViewType.setText(R.string.manual);
                    this.editTextOpen.setEnabled(true);
                    this.btnSend.setEnabled(true);
                }
                break;
            case OPEN:
                this.editTextOpenActual.setText(resources.getString( R.string.opens,string));
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + msg.what);
        }
    }
}